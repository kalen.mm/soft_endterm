package main

import (
	"context"
	"gitlab.com/kalen.mm/soft_endterm/CalculatorEndterm/calculator/calculator_pb"
	"google.golang.org/grpc"
	"log"
	"net"
)

type Server struct {
	calculator_pb.UnimplementedCalculatorServiceServer
}

func (s *Server) PrimeNumberDecomposition(ctx context.Context, req *calculator_pb.PrimeNumberDecompositionRequest) (*calculator_pb.PrimeNumberDecompositionResponse, error) {
	var count []int32
	numberR := req.GetNumber()

	for numberR%2 == 0 {
		count = append(count, 2)
		numberR = numberR / 2
	}

	for i := 3; int32(i*i) <= numberR; i = i + 2 {
		for numberR%int32(i) == 0 {
			count = append(count, int32(i))
			numberR = numberR / int32(i)
		}
	}

	if numberR > 2 {
		count = append(count, numberR)
	}

	res := &calculator_pb.PrimeNumberDecompositionResponse{
		Answer: count,
	}

	return res, nil
}

func (s *Server) ComputeAverage(ctx context.Context, req *calculator_pb.ComputeAverageRequest) (*calculator_pb.ComputeAverageResponse, error) {
	numbers := req.GetNumbers()
	var sum int32
	for i := 0; i < len(numbers); i++ {
		sum += numbers[i]
	}

	avg := float32(sum) / float32(len(numbers))

	res := &calculator_pb.ComputeAverageResponse{
		Answer: avg,
	}

	return res, nil
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:5000")
	if err != nil {
		log.Fatalf("Failed to listen :( - %v", err)
	}
	s := grpc.NewServer()

	calculator_pb.RegisterCalculatorServiceServer(s, &Server{})
	log.Println("Server is running on port:5000")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve :( - %v", err)
	}
}
