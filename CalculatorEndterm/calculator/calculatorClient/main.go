package main

import (
	"bufio"
	"context"
	"fmt"
	"gitlab.com/kalen.mm/soft_endterm/CalculatorEndterm/calculator/calculator_pb"
	"google.golang.org/grpc"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	var num []int32
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Write whole one integer for decompositions(example 120)\nWrite separate numbers for getting average(1 2 3 4 5)\nSenpai, please enter the num(s) : ")

	numbers, _ := reader.ReadString('\n')

	reg := regexp.MustCompile(`[-]?\d[\d,]*[\.]?[\d{2}]*`)
	convStr := reg.FindAllString(numbers, -1)

	for _, unit := range convStr {
		temp, err := strconv.ParseInt(unit, 10, 32)
		if err != nil {
			log.Fatal(err)
		}
		num = append(num, int32(temp))
	}

	conn, err := grpc.Dial("localhost:5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := calculator_pb.NewCalculatorServiceClient(conn)

	if len(num) == 1 {
		PrimeNumberDecomposition(c, num[0])
	} else {
		ComputeAverage(c, num)
	}
}

func PrimeNumberDecomposition(c calculator_pb.CalculatorServiceClient, num int32) {
	contextS := context.Background()
	request := &calculator_pb.PrimeNumberDecompositionRequest{
		Number: num,
	}

	response, err := c.PrimeNumberDecomposition(contextS, request)

	if err != nil {
		log.Fatalf("Error happend ^_^ when computing decomposition%v", err)
	}

	log.Println(response.Answer)
}

func ComputeAverage(c calculator_pb.CalculatorServiceClient, nums []int32) {
	contextS := context.Background()
	request := &calculator_pb.ComputeAverageRequest{
		Numbers: nums,
	}

	response, err := c.ComputeAverage(contextS, request)

	if err != nil {
		log.Fatalf("Error happend ^_^ when computing average value%v", err)
	}

	log.Println(response.Answer)
}
